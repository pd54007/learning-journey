# Assert和适用场景 大纲 索引
* 继承TestCase即可适用，因为其继承于PHPUnit\Framework\Assert

### 常见
#####1. 相等
- assertEquals()
- assertNotEquals()
如果是浮点类型，可以设置比较的精度
如果是object，属性不同就不等
也可以比较array是否一样
- assertSame() 值与类型都要相同
#####2. 判断是否返回ture/false
- assertFalse()
- assertTrue()
#####3. 比较大小
- assertGreaterThan()
- assertGreaterThanOrEqual()
- assertInfinite() 超过范围
- assertLessThan()
- assertLessThanOrEqual()
- assertNan() 是否是NAN
- assertNull()
#####4. 判断类型
- assertInternalType()

### Array相关
#####1. array里是否存在某个Key
- assertArrayHasKey()
- assertArrayNotHasKey()
#####2. array里是否有子集
- assertArraySubset()
#####3. array里是否有某个值
- assertContains() 可设置忽略大小写，也适用于判断子字符串
- assertNotContains()
#####4. array里的元素是否是同一个type
- assertContainsOnly()
- assertNotContainsOnly()
#####5. 长度
- assertCount()
#####6. 空
- assertEmpty()
- assertNotEmpty()
#####7 正则
- assertRegExp()
- assertStringMatchesFormat()

### String相关
- assertStringEndsWith()
- assertStringStartsWith()

### Class相关
#####1. Class与attribute有关的操作
- assertClassHasAttribute()
- assertClassNotHasAttribute()
- assertClassHasStaticAttribute()
- assertClassNotHasStaticAttribute()
- assertAttributeContains()
- assertAttributeNotContains()
- assertAttributeContainsOnly()
- assertAttributeNotContainsOnly()
- assertAttributeEmpty()
- assertAttributeNotEmpty()
- assertAttributeGreaterThan()
- assertAttributeGreaterThanOrEqual()
- assertAttributeInstanceOf()
- assertAttributeInternalType()
- assertObjectHasAttribute() $obj->attribute是否有
#####2. 是否是实例对象
- assertInstanceOf()

### 文件和目录
#####1. 目录存不存在
- assertDirectoryExists()
- assertDirectoryNotExists()
#####2. 目录是不是个目录并且是否可读,是否可写
- assertDirectoryIsReadable()
- assertDirectoryNotIsReadable()
- assertDirectoryIsWritable()
- assertDirectoryNotIsWritable()
#####3. 文件是否一样
- assertFileEquals()
- assertFileNotEquals()
#####4. 文件是否存在
- assertFileExists()
- assertFileNotExists()
#####5. 文件是否可读可写
- assertFileIsReadable()
- assertFileNotIsReadable()
- assertFileIsWritable()
- assertFileNotIsWritable()
#####6. 不区分文件和目录的用法
- assertIsReadable()
- assertIsWritable()
#####7. 比较文件的内容
- assertStringMatchesFormatFile()
- assertStringEqualsFile

### 自我感觉不常用
- assertContainsOnlyInstancesOf()
- assertEqualXMLStructure() 两个xml结构是否一致，可选择是否检查属性
- assertXmlFileEqualsXmlFile()
- assertXmlStringEqualsXmlFile()
- assertXmlStringEqualsXmlString()
- assertJsonFileEqualsJsonFile()
- assertJsonStringEqualsJsonFile()
- assertJsonStringEqualsJsonString()
- assertThat() 组合拳