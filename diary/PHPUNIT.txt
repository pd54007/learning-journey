PHPUNIT 

- @depends
When using @depends, the sequence is important, otherwise, the system will skip some test cases.  不存在拷贝，被依赖的值直接pass给了需要用到的另一个方法。 第二个方法其实就是接受的前面传进来的参数。如果是多个参数，那么顺序一致就行。

- @dataProvider 
接参数的时候，无论写的顺序如何，这个相关的总是排在@depends前面
用一个数组  或者一个实现了iterator interface的对象来设置一系列的数值.
public function additionProvider()
{
    return [
        'adding zeros'  => [0, 0, 0],
        'zero plus one' => [0, 1, 1],
        'one plus zero' => [1, 0, 1],
        'one plus one'  => [1, 1, 3]
    ];
}

- 
setUp() tearDown()
在每一个case执行的前后都会执行

- 
setUpBeforeClass() tearDownAfterClass()
运行于某一个class里的第一个case之前，和最后一个case结尾
常用于链接数据库

PHPUnit 提供的 createMock($type) 和 getMockBuilder($type) 方法可以在测试中用来自动生成对象，此对象可以充当任意指定原版类型（接口或类名）的测试替身。在任何预期或要求使用原版类的实例对象的上下文中都可以使用这个测试替身对象来代替。

mock vs stub:
	Stub: 已经有方法了，把方法的返回值或者别的改掉
	Mock: A mock is something that as part of your test you have to setup with your expectations. 测试mock这个方法被如何地调用

可以用一个framework Prophecy来实现。
可以Mock web service 
可以mock filesystem


