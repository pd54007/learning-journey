# Annotations 大纲 索引
必须以`/**`开头，以code `*/` 结尾

### 生命周期
- @beforeClass
- @before 该方法用在每个test之前
- @after 该方法用在每个test之后
- @afterClass 该静态方法运行在类中所有的test结束之后
类似于普通生命周期函数
setUpBeforeClass()
setUp()
tearDown()
tearDownAfterClass()

### 主要
- @dataProvider 提供给方法参数
- @depends 用指定的这个的结果
- @testWith 只定义一组测试数据

### 异常相关
- @expectedException
- @expectedExceptionCode
- @expectedExceptionMessage
- @expectedExceptionMessageRegExp

### 备份还原的操作，用于测试完之后恢复
- @backupGlobals 可以用在类或者方法上
- @backupStaticAttributes 可以用在类或者方法上

### 测试覆盖率相关
- @codeCoverageIgnore
- @codeCoverageIgnoreStart
- @codeCoverageIgnoreEnd
- @covers 指明测试的哪个方法，或哪些方法
- @coversDefaultClass
- @coversNothing
- @uses 测试的时候执行到，但是不让其被测试覆盖

### 进程相关
- @preserveGlobalState 运行在别的进程上的东西不能改全局的状态
- @runTestsInSeparateProcesses 指明一个测试类
- @runInSeparateProcesses 指明某个测试

### 暂时用不上的
- @author
- @test 这样就不用写test为方法的前缀
- @group 标记属于什么组
- @ticket group的别名
- @large 与测试用时相关
- @medium
- @small
- @requires 提前判断条件 不满足则不测
- @testdox 生成doc的时候用到
