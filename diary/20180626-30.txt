26：
1 商家选择store，如果只有一个，那就不需要手动选，实现这个逻辑
2 增加input控件在扫描二维码的下面
3 写工作小结
4 自学Symfony4

27：
1 AccountEdit页面制作
2 Input值的检测，都用state来做，主要是格式方面的检查，防止用户发送垃圾request到后台。如果涉及到后台检测，则可以用this属性来做
3 一些固定的const变量，比如澳洲所有state（7种），性别种类，这些都可以放在前端的Dict文件夹下面。
4 学习闭包closures，js的class其实也是用闭包实现的，内部是copy了一个传入的object。用法可以参照AccountEdit页面里的使用。
5 var和let的scoping是不同的，前者是可以在同一个module里使用的，后者限制在当前scoping里
6 封装CustomTextField,主要是用于实现picker，不用input里面的方式是防止以后material ui 自己实现原生的日期选择界面
例子
<!DOCTYPE html>
<html>
<body>

<h2>JavaScript Closures</h2>

<p>Counting with a local variable.</p>

<button type="button" onclick="myFunction()">Count!</button>

<p id="demo">0</p>

<script>
var add = (function (counter=0) {
    counter=10;
    return function () {counter += 1; return counter;}
})();

function myFunction(){
    document.getElementById("demo").innerHTML = add();
}
</script>

</body>
</html>

点击按钮从11开始变化

28：
给AccountEdit页面增加了error message的显示。
重新封装了Textfield 因为它提供了Props  并没有给CSS API， 所以不需要用mui的方式来封装。
重做注册流程，注册之后直接登录。
修改注册时候的密码提示（变红）。
拉商户注册。

29：
重新制作客户Credit页面。
修改transactions list的bug。比如跳转placeholder bug，和数量不对的bug
当只有1页内容的时候就不要pagenation bar
增加了按键回车提交表格的功能
sub dialog重新设计制作

30：
约谈商家失败。已发邮件解释原因。
